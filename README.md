
# NmeaStar

*Cross platform C++ 17 NMEA Parser & GPS Framework*

NmeaStar is a NMEA parser based on Nematode
It comes with a GPS service class to handle the most popular GPS NMEA sentences.

The original library can be found at:
https://github.com/ckgt/NemaTode

This library also includes changes made by user Chuvi-w, as their fork contained additional improvements to the library.
Their fork of NemaTode can be found here:
https://github.com/Chuvi-w/NemaTode

----
##Features

* **NMEA Reading** of standard and custom sentences.

  - Standard:
```` 
    GPGGA, GPGSA, GPGSV, GPRMC, GPVTG
````

* **NMEA Generation** of "standard" and custom sentences.
  - SiRF Control sentences: ```` PSRF100, PSRF103 ````

* **GPS Fix** class to manage and organize all the GPS related data.


* **Flexible**
   - Stream data directly from a hardware byte stream
   - Read log files, even if they are cluttered with other unrelated data.
   - Easily hook into the parser to handle custom sentences and get their data.
   - Simplified GPS data
    
* **C++ 17 **
   - If you are on an embedded system... sorry. This might not work for you because of compiler restrictions.
   - GPS service class should hopefully be threadsafe

## Details
NMEA is very popular with GPS. Unfortunately the GPS data is fragmented
between many different NMEA sentences. Sometimes there is even conflicting 
information in them!

The underlying **NMEA Reader** can read a byte stream from a hardware device or 
it can read log files with flexibility in the formatting. The accepted strings are...

````
"$[name:alphanum],[param[i]:alphanum],...(*alphanum[2]\r)\n"
````

The params allow '-' and any combination of whitespace.

The handler for "MYNMEA" is called. This is where you can catch the sentence and process it.

    reader.SetSentenceHandler("MYNMEA",[](const Sentence& nmea){
        if( ! nmea.ChecksumOK() ){
            // bad checksum, but valid syntax
        }
        if( nmea.GetParameters().size() < 3 ){
            // missing data, throw something.
            // catch it at the read*() call.
        }
        int mydata = utility::ParseInt(nmea.parameters[2]);
    };



There are 2 ways to operate...

* **LAX**  It will eat anything.
    - Useful for reading log files.
    - Call ````ReadByte(), ReadBuffer(), ReadLine()````
* **STRICT**   It will throw errors on anything that's not explicitly NMEA.
    - Call ```` ReadSentence() ````


## Demos
**"demo_simple.cpp"**

 * Just reads the GPS position data.


**"demo_advanced.cpp"**

 * Reads **all** the data
 * Sentence Generation
 * Custom Sentence handling

**Generation**
    
    Command mycmd{"MYCMD", "your,data,csv"};
    string nmea = mycmd.ToString();  // Creates: $MYCMD,your,data,csv*15


There are 2 included NMEACommands that can configure a GPS.

 * ```` PSRF103```` Configures message output ID and rate, and whether or not to use checksums.

 * ```` PSRF100```` Configures the UART serial connection (if the chip has one).


## Include Nmeastar in your project
You can include NemaStar via [CMake](https://cmake.org) in our project.
Your basic CMakeLists.txt file could look like:

    cmake_minimum_required(VERSION 3.13)
    project(MyProject)

    find_package(nmeastar REQUIRED CONFIG)

    add_executable(${PROJECT_NAME} mycode.cpp)

    target_link_libraries(${PROJECT_NAME} nmeastar::nmeastar)


## GPS Fix data available
Available data when all 5 GPS sentences are received. If some are missing then some parameters will never change from their default values.
All data is checked for consistency. For example, visible satellites can never be less than the tracking satellites, etc.

## TODO
* The fix class needs more work related to member varialbe access. As currently only the service class can access it propperly
* Add more standard nmea sentences to the service
* Add more functionality regarding accuracy & precision
* Add more tests
* Add more documentation
* Issue: Cmake using already installed headers when building