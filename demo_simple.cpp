/*
 *
 *  See the license file included with this source.
 */
 
#include <iostream>
#include <fstream>
#include <iomanip>

#include "nmeastar/nmeastar.h"

namespace
{

nmeastar::gps::Precision chip_pres(3.0, 50.0); //m, %

void OnUpdate(const nmeastar::gps::Fix& fix)
{
  std::cout << (fix.Locked() ? "[*] " : "[ ] ") << std::setw(2) << std::setfill(' ') << fix.GetTrackingSatellites() << "/" << std::setw(2) << std::setfill(' ') << fix.GetVisibleSatellites() << " ";
  std::cout << std::fixed << std::setprecision(2) << std::setw(5) << std::setfill(' ') << fix.GetAlmanac().AverageSNR() << " dB   ";
  std::cout << std::fixed << std::setprecision(2) << std::setw(6) << std::setfill(' ') << fix.GetPosition().speed_ << " km/h [" << nmeastar::gps::Fix::TravelAngleToCompassDirection(fix.GetPosition().travel_angle_, true) << "]  ";
  std::cout << std::fixed << std::setprecision(6) << fix.GetPosition().latitude_ << "\xF8 " "N, " << fix.GetPosition().longitude_ << "\xF8 " "E" << "  ";
  std::cout << "+/- " << std::setprecision(1) << fix.HorizontalAccuracy(chip_pres) << "m  ";
  std::cout << std::endl;
}

}


int main(){

	// Fill with your NMEA bytes... make sure it ends with \n
	char bytestream[] = "\n";

	// Create a GPS service that will keep track of the fix data.

	nmeastar::nmea::Reader reader{};
	nmeastar::gps::Service gps(reader);

  //Lambda for priting raw sentences
  reader.OnSentence = [](const nmeastar::nmea::Sentence & nmea)
  {
    std::cout << "Reading: " << nmea.GetRawSentence() <<std::endl;
  };

  //Set lambda for printing errors
  gps.OnError = [](std::string error)
  {
    std::cout << error <<std::endl;
  };

	// Handle any changes to the GPS Fix... This is called whenever it's updated.
  nmeastar::gps::Observer observer(gps,
    [](bool state)
  {
    std::cout << "Lock State changed: " << (state ? "[*] " : "[ ] ") << std::endl;
  }, &OnUpdate);  

  //Cout header
  std::cout << "Fix  Sats  Sig\t\tSpeed    Dir  Lat         , Lon           Accuracy" << std::endl;

	// -- STREAM THE DATA  ---

	// From a buffer in memory...
	reader.ReadBuffer((uint8_t*)bytestream, sizeof(bytestream));

	// -- OR --
	// From a device byte stream...
	// gps.parser.readByte(byte_from_device);

  ///TODO: Make nmeal_log.txt a main argument

	// -- OR --
	// From a file
	std::string line;
	std::ifstream file("../nmea_log.txt");
	while (std::getline(file, line)){
		reader.ReadLine(line);
	}

	// Show the final fix information
	std::cout << gps.GetFix().ToString(::chip_pres) << std::endl;


	std::cin.ignore();


	return 0;
}