/*
 *
 *  See the license file included with this source.
 */

#include <iostream>
#include <fstream>

#include "nmeastar/nmeastar.h"
#include "nmeastar/utility.h"

#include "nmeastar/nmea/command/queryrate.h"
#include "nmeastar/nmea/command/serialconfiguration.h"

namespace
{
  void OnLockStateChanged(bool lock)
  {
    if (lock){
			std::cout << "\t\t\tGPS aquired LOCK!" << std::endl;
		}
		else {
			std::cout << "\t\t\tGPS lost lock. Searching..." << std::endl;
		}
  }

  void OnUpdate(const nmeastar::gps::Fix& fix)
  {
    std::cout << "\t\t\tPosition: " << fix.GetPosition().latitude_ << "'N, " << fix.GetPosition().longitude_ << "'E" << std::endl << std::endl;
  }
}

int main(){



	// --------------------------------------------------------
	// ------------  CONFIGURE GPS SERVICE  -------------------
	// --------------------------------------------------------
	
	// Create a GPS service that will keep track of the fix data.
	nmeastar::nmea::Reader reader{};
	nmeastar::gps::Service gps(reader);

	// Handle events when the lock state changes
  nmeastar::gps::Observer observer{gps, &OnLockStateChanged, &OnUpdate};

	// (optional) - Handle events when the parser receives each sentence
	reader.OnSentence = [&gps](const nmeastar::nmea::Sentence& n){
		std::cout << "Received " << (n.ChecksumOK() ? "good" : "bad") << " GPS Data: " << n.GetCommandName() << std::endl;
	};


	std::cout << "-------- Reading GPS NMEA data --------" << std::endl;

	// --------------------------------------------------------
	// ---------------   STREAM THE DATA  ---------------------
	// --------------------------------------------------------
  // From a buffer in memory...
  //   cout << ">> [ From Buffer]" << endl;
  //   parser.readBuffer((uint8_t*)bytestream, sizeof(bytestream));
  // ---------------------------------------

  // -- OR --
  // From a device byte stream...
  //   cout << ">> [ From Device Stream]" << endl;
  //   parser.readByte(byte_from_device);
  // ---------------------------------------

  // -- OR --
  // From a text log file...
  std::cout << ">> [ From File]" << std::endl;
  std::string line;
  std::ifstream file("nmea_log.txt");
  while ( std::getline(file, line) ){
    reader.ReadLine(line);
  }
	// ---------------------------------------


	// Show the final fix information
	//cout << gps.fix.toString() << endl;





	// --------------------------------------------------------
	// ---------------   NMEA ALTERNATIVE SENTENCES  ----------
	// --------------------------------------------------------
	// Not using GPS NMEA Sentences? That's A-OK.
	// While there is no data aggregation code written here for 
	// non-GPS use, the parser will still make your job easier.
	// Extract only the sentences you care about.

	// Create our custom parser...
	nmeastar::nmea::Reader custom_reader;
	//parser.log = true;
	custom_reader.SetSentenceHandler("MYNMEA", [](const nmeastar::nmea::Sentence& n){
		std::cout << "Handling $" << n.GetCommandName() << ":" << std::endl;
		for (size_t i = 0; i < n.GetParameters().size(); ++i){
			std::cout << "    [" << i << "] \t- " << n.GetParameters()[i];
      double num = nmeastar::utility::ParseDouble(n.GetParameters()[i]);
      std::cout << "      (number: " << num << ")";
			std::cout << std::endl;
		}
	});
	custom_reader.OnSentence = [](const nmeastar::nmea::Sentence& n){
		std::cout << "Received $" << n.GetCommandName() << std::endl;
	};

	std::cout << "-------- Reading non-GPS NMEA data --------" << std::endl;

	// Read the data stream...
	// These don't have correct checksums. They're made up.
	const std::string data =
		"  $MYNMEA,1,3,3,7,Hello*A2\n		\
			$IRRELEVANT,5,5,5*AA\n			\
			$ERRORS,:D,\n					\
			$\n								\
			$$\n							\
			$*\n							\
			$*,\n							\
			$,\n							\
			$,*\n							\
			garbage that will be			\
			!IgN0r3d @)(&%!!!				\
			$MYNMEA,1,3,3,7,World!*A2\r\n	\
			";
	for (const char byte : data) 
  {
		custom_reader.ReadByte(byte);
	}





	// --------------------------------------------------------
	// ---------------   NMEA SENTENCE GENERATION  ------------
	// --------------------------------------------------------
	// Some devices allow control sentences to be sent to them.
	// For some GPS devices this can allow selecting certain data.
	// Only the following 2 Sentences are implemented, however
	// you can create your own from the NMEACommand base class.

  ///TODO: A lot of the code for this has not been refactored propperly yet and is missing
  ///TODO: constuctors and interfaces that make it easily usable

	// Test the parser and command generation

  // A blank generic command, Just filling it with something. Could be whatever you need.
	nmeastar::nmea::command::Command cmd1{"CMD1", "nothing,special"};

	//nmeastar::nmea::command::QueryRate				      cmd2;	// The $PSRF command that allows for GPS sentence selection and rate setting.

	//nmeastar::nmea::command::QueryRate				      cmd3;	// The $PSRF command that allows for GPS sentence selection and rate setting.

	//nmeastar::nmea::command::SerialConfiguration		cmd4;	// The $PSRF command that can configure a UART baud rate.
	nmeastar::nmea::Reader test_parser;
	test_parser.OnSentence = [&cmd1](const nmeastar::nmea::Sentence& n){
		std::cout << "Received:  " << n.GetRawSentence();
		
		if (!n.ChecksumOK()){
			std::cout << "\t\tChecksum FAIL!" << std::endl;
		}
		else {
			std::cout << "\t\tChecksum PASS!" << std::endl;
		}
	};

	std::cout << "-------- NMEA Command Generation --------" << std::endl;

	// Config output rate for $GPGGA sentence
	//cmd2.messageID	= nmeastar::nmea::command::QueryRate::MessageID::GGA;
	//cmd2.mode		= nmeastar::nmea::command::QueryRate::QueryRateMode::SETRATE;
	//cmd2.rate		= 3;		// output every 3 seconds, 0 to disable

	// Query $GPGSV almanac sentence just this once
	//cmd3.messageID	= nmeastar::nmea::Sentence::MessageID::GSV;
	//cmd3.mode		= NMEACommandQueryRate::QueryRateMode::QUERY;

	// Set the Baud rate to 9600, because this GPS chip is awesome
	//cmd4.baud		= 9600;		// 4800 is NMEA standard

	// Generate the NMEA sentence from the commands and send them back into the test parser.
	test_parser.ReadSentence(cmd1.ToString());
	//test_parser.ReadSentence(cmd2.ToString());
	//test_parser.ReadSentence(cmd3.ToString());
	//test_parser.ReadSentence(cmd4.ToString());



	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "-------- ALL DONE --------" << std::endl;



	std::cin.ignore();

	return 0;

}