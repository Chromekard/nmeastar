/**
 * @file utility.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 14, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <cstdint>
#include <string>

namespace nmeastar::utility
{
  double ParseDouble(const std::string& s);
  int64_t ParseInt(const std::string& s, int radix = 10);
} // namespace nmeastar
