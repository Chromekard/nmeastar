/**
 * @file nmeastar.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date March 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

// The implementation of a NMEA 0183 parser.
// The implementation of a NMEA 0183 sentence generator.
// The implementation of a GPS data service.

#pragma once

#include "nmeastar/gps/service.h"
#include "nmeastar/gps/watch/observer.h"
#include "nmeastar/nmea/command/command.h"