/**
 * @file precision.h
 * @author Bjørn Fuglestad
 * @date August 31, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

namespace nmeastar::gps
{

/**
 * Precision class stores information regarding the gps chips
 * documented precision.
 * @todo might want to make a union or something as accuracy might
 * change if gps has access to DGPS or any other position enhancing
 * technology
 * 
 * The accuracy of a device will depend on the precision of the chip:
 * the precision being the % of hits that were within a radius(r) of the meadian position
 * Term 	Definition 	                    Precision
 * CEP 	  Circular Error Probable 	      50%
 * RMS 	  Root Mean Square 	              63-68%
 * 2DRMS 	Two times the distance of RMS   95-98%
 * R95 	  Radius 95% 	                    95%
 * @todo maybe make a template precision
*/
class Precision
{
public:

  /**
   * @todo Might want to add other accuracy measurement types such as
   * circular error probable (CEP) and R95 which is the radius of 
   * the circle where 95% of the values would fall in.
   * CEP 	  Circular Error Probable 	      50%
   * RMS 	  Root Mean Square 	              63-68%
   * 2DRMS 	Two times the distance of RMS   95-98%
   * R95 	  Radius 95% 	                    95%
   * But dimmentions is also an issue
   */
  enum class Term
  {
    CEP,
    SEP,
    RMS,
    _2DRMS,
    R95
  };

  /**
   * @param error_radius that the true position could be in
   * @param percentage of points that are within the error radius
  */
  Precision(const float error_radius, const float percentage) : 
    error_radius_h_(error_radius), error_radius_v_(error_radius), percentage_(percentage)
  {}

  /**
   * @param error_radius_h that the true position could be in (horizontal)
   * @param error_radius_v that the true position could be in (vertical), tends to be less accurate by 1.7
   * @param percentage of points that are within the error radius
  */
  Precision(const float error_radius_h, const float error_radius_v, const float percentage) : 
    error_radius_h_(error_radius_h), error_radius_v_(error_radius_v), percentage_(percentage)
  {}
  
  const float error_radius_h_{3.0};         // Horizontal error circle
  const float error_radius_v_{3.0 * 1.7};   //
  const float percentage_{50.0};            //Percentage of points that are withing circle

  ///TODO: Implement radius as a union or something?

  ///TODO: Implement conversion between percentages, plus figure out how to handle
  /// vertical (1d), horizontal(2d) and 3d dimentions
  /// https://en.wikipedia.org/wiki/Circular_error_probable
  /// https://www.gpsworld.com/gps-accuracy-lies-damn-lies-and-statistics/
};

}