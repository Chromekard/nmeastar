/**
 * @file observer.h
 * @author Bjørn Fuglestad
 * @date Mai 27, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#pragma once

#include <functional>

#include "subject.h"
#include "../fix.h"

namespace nmeastar::gps
{

/**
 * Class storing callbacks for observing the gps service and getting information
 * The main purpose of the class is to ensure that the observer functions are RAII
 * compliant.
 * 
 * @note An alternative to this approach would be to have some function register class
 * that would store each object and then handle RAII for each function. As was implemented
 * in the original library. Maybe boost or some other library could be added as a dependency
 * to provide this functionality in the future
*/
class Observer
{
public:

 /**
   * Constructor
   * @param observed object
   * @param state_lock_changed callback for when satelite lock state changes
   * @param on_update callback for when gps data is updated
   */
  Observer(Subject& observed, 
    std::function<void(bool)> on_state_lock_changed, 
    std::function<void(const Fix&)> on_update) : 
    observed_(observed),
    on_state_lock_changed_(on_state_lock_changed), 
    on_update_(on_update)
  {
    observed_.Attach(this);
  }

  ~Observer()
  {
    observed_.Detach(this);
  }

private:

  Subject& observed_;

public:

  /**
   * Callback for when satelite lock state changes
   */
  std::function<void(bool)> on_state_lock_changed_{};

  /**
   * callback for when gps data is updated
   * @param fix - const reference to a local copy of fix for thread safety
   * and to prevent any locking issues
  */
  std::function<void(const Fix&)> on_update_{};

};

}