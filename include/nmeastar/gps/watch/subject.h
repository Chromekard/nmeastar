/**
 * @file observer.h
 * @author Bjørn Fuglestad
 * @date August 31, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <set>
#include <mutex>

namespace nmeastar::gps
{

//Foward declare to prevent circular dependecy
class Observer;

/**
 * 
*/
class Subject
{
public:

  virtual ~Subject(){}

  /**
   * Attach an observer to the subject
   * @param observer to attach
  */
  void Attach(Observer* const observer)  
  {
    std::scoped_lock<std::mutex> lock(observer_mtx_);
    observer_set_.insert(observer);
  }

  /**
   * Detach an observer from the subject
   * @param observer to detach
  */  
  void Detach(Observer* const observer)  
  {
    std::scoped_lock<std::mutex> lock(observer_mtx_);
    observer_set_.erase(observer);
  }

  virtual void NotifyLock() = 0;
  virtual void NotifyFix() = 0;

protected:

  std::set<Observer*> observer_set_{}; //Set of different observers
  mutable std::mutex observer_mtx_{};               //Mutex for controlling access to observer set iteration
};

}