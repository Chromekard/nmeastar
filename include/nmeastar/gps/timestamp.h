/**
 * @file timestamp.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <cstdint>
#include <string>

namespace nmeastar::gps
{

/**
 * Class for annotating the gps timestamps
*/
class Timestamp
{
public:
  Timestamp(){};

  time_t GetTime() const;

  double GetRawTime() const
  {
    return raw_time_;
  }

  int32_t GetRawDate() const
  {
    return raw_date_;
  }

  /**
   * Set time directly from the NMEA time stamp
   * hhmmss.sss
   */
  void SetTime(double raw_ts);

  /**
   * Set date directly from the NMEA time stamp
   * ddmmyy
   */
  void SetDate(int32_t raw_date);

  std::string ToString() const;

private:
  std::string MonthName(uint32_t index) const;

  int32_t hour_{};
  int32_t min_{};
  double sec_{};

  int32_t month_{1};
  int32_t day_{1};
  int32_t year_{1970};

  // Values collected directly from the GPS
  double raw_time_{};
  int32_t raw_date_{};

};

}