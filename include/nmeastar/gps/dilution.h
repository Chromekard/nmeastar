/**
 * @file dilution.h
 * @author Bjørn Fuglestad
 * @date August 31, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

namespace nmeastar::gps
{

/**
 * Struct containg the dilition of precision (DOP) caused by satelite positions.
 * When visible navigation satellites are close together in the sky, 
 * the geometry is said to be weak and the DOP value is high; when far apart, 
 * the geometry is strong and the DOP value is low. 
 * DOP values are measeured in a range of best = <1, worst = >20.
 * Values less than 10 provide measure ments that could be used for calculations.
 * If DOP is above 10 position measurements should only be used for rough estimates
 * If the value if over 20 measurements as much as 300 meters 
 * with a 6-meter accurate device (50 DOP × 6 meters)
 * @todo make into template?
*/
struct Dilution
{
  double position_   {100}; // PDOP	– position (3D) dilution of precision, combination of Vertical & Horizontal
  double horizontal_ {100}; // HDOP – horizontal dilution of precision
  double vertical_   {100};	// VDOP – vertical dilution of precision, 
                            // Vertical is less accurate than horizontal (h * 1.7), and not used in most application
  
  ///TODO: Implement?
  //double time_       {100}; // TDOP – time dilution of precision
  //double geometric_  {100}; // GDOP – geometric dilution of precision
};

}