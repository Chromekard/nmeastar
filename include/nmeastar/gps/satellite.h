/**
 * @file satellite.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <cstdint>
#include <string>

namespace nmeastar::gps
{

/**
 * @brief 
 * @
*/
class Satellite
{
public:
  Satellite(){};
  Satellite(uint32_t prn, double snr, double elevation, double azimuth);

  /**
   * Convert class data to string
   * @return data as string
  */
  std::string ToString() const;
  operator std::string();

  /**
   * @Set elevation
   * @param elevation value between 0-90 deg
  */
  void SetElevation(double elevation);

  /**
   * @brief Set azimuth
   * @param azimuth, value between 0-359 deg
  */
  void SetAzimuth(double azimuth);

  /**
   * @brief get PRN
   * @return PRN, value between 0-32
  */
  uint32_t GetPRN() const
  {
    return prn_;
  }

  /**
   * @brief Get SNR
   * @return SNR, value between 0-99 dB
  */
  double GetSNR() const
  {
    return snr_;
  }

  /**
   * @brief Get elevation
   * @return elevation, value between 0-90 deg
  */
  double GetElevation() const
  {
    return elevation_;
  };

  /**
   * @brief Get azimuth
   * @return azimuth, value between 0-359 deg
  */
  double GetAzimuth() const
  {
    return azimuth_;
  };

private:
  //satellite data
  uint32_t prn_{};     // id - 0-32
  double snr_{};       // 0-99 dB
  double elevation_{}; // 0-90 deg
  double azimuth_{};   // 0-359 deg
};

} // namespace nmea