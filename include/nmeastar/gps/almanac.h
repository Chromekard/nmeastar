/**
 * @file almanac.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Jul 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <vector>
#include <cstdint>

#include "satellite.h"

namespace nmeastar::gps
{

/**
 * Class containing satelite almanac data.
*/
class Almanac
{
public:

  ///TODO: Might need to give better access if we want custom sentence handling to be implemented
  //Give GPSService access to private varialbes to allow it to change values based on parsed data
  friend class Service;

  Almanac(){};

  /**
   * @brief Get average SNR
   * @return arerage SNR
  */
  double AverageSNR() const;

  /**
   * @brief Get minimum SNR
   * @return minimum SNR
  */
  double MinSNR() const;

  /**
   * @brief Get maximum SNR
   * @return maximum SNR
  */
  double MaxSNR() const;

  /**
   * @brief Get average SNR
   * @return true if locked, false if not
  */
  double PercentComplete() const;

  /**
   * @brief Remove all information from the satellites
   */
  void Clear();

  /**
   * 
   */
  void UpdateSatellite(Satellite sat);

  ///TODO: See about making this private as we dont want dirrect access

  //mapped by prn
  std::vector<Satellite> satellites_{};

private:

  uint32_t visible_size_{};
  uint32_t last_page_{};
  uint32_t total_pages_{};
  uint32_t processed_pages_{};
};
}