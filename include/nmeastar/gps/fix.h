/**
 * @file fix.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Jul 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <chrono>
#include <string>

#include "precision.h"
#include "dilution.h"
#include "position.h"
#include "almanac.h"
#include "timestamp.h"

namespace nmeastar::gps
{


/**
 * @mainpage
 * @brief Class for handling gps fix
*/
class Fix
{

  //Give GPSService access to private varialbes to allow it to change values based on parsed data
  friend class Service;

public:

  /**
   * 
   */
  enum class Type : std::uint8_t
  {
    none = 1,
    _2d = 2,
    _3d = 3
    // Type: 1=none, 2=2d, 3=3d
  };

  /**
   * 
   */
  enum class Quality : std::uint8_t
  {
    invalid,
    gps_fix,
    dgps_fix,
    pps_fix,
    rtk,
    float_rtk,
    estimated
  };

  static std::string TravelAngleToCompassDirection(double deg, bool abbrev = false);

  Fix();
  virtual ~Fix() = default;

  /**
   * @brief check if a gps fix has been obtained
   * @return true if locked, false if not
  */
  bool Locked() const
  {
    return haslock_;
  }
  
  /**
   * @brief 
   * @return 
  */
  bool HasEstimate() const
  {
    return (position_data_.latitude_ != 0 && position_data_.longitude_ != 0) 
      || (quality_ == Quality::estimated);
  }


    /**
   * @brief get horizontal accuracy
   * @param h2drms (horizontal twice distance root mean square) 
   * get the vale from GPS CHIP datasheet: horizontal 2drms 95% = 4.0
   * @return horizontal accuracy in meters
  */
  double HorizontalAccuracy(const Precision& chip_precision) const
  {
    return chip_precision.error_radius_h_ * dilution_data_.horizontal_;
  }
  
  /**
   * @brief get vertical accuracy, preffer horizontal
   * @param v2drms (vertical twice distance root mean square) 
   * get the vale from GPS CHIP datasheet: vertical 2drms 95% = 6.0
   * @return vertical accuracy in meters
  */
  double VerticalAccuracy(const Precision& chip_precision) const
  {
    return chip_precision.error_radius_v_ * dilution_data_.vertical_;
  }

  /**
   * @brief get time since last update
   * @return seconds since last update
  */
  std::chrono::seconds TimeSinceLastUpdate() const; // Returns seconds difference from last timestamp and right now.

  /**
   * @todo consider if precision should be stored in the fix, service or completly outside as it is now?
  */
  std::string ToString(const Precision& chip_precision) const;

  //operator std::string();

  /// Getters ///

  const Position& GetPosition() const 
  {
    return position_data_;
  }
  
  const Dilution& GetDilution() const 
  {
    return dilution_data_;
  }

  const Almanac& GetAlmanac() const 
  {
    return almanac_;
  }

  const Timestamp& GetTimestamp() const 
  {
    return timestamp_;
  }

  
  int32_t GetTrackingSatellites() const
  {
    return tracking_satellites_;
  }

  int32_t GetVisibleSatellites() const
  {
    return visible_satellites_;
  }

  ///Setters
  ///Allowing setters in the event that a user
  ///would want to use the class with their own
  ///implementation. As otherwise service is the only
  ///class with access to changing the internal state
  ///of this class. In any case since service should
  ///only return copies there is no issue with
  ///the fix it using being changed by the user directly

  ///TODO: Add more setters

  /**
   * @brief set the lock value
   * @return true if lock status **changed**, false otherwise.
  */
  bool SetLock(bool b);

  ///TODO: Do we need the setters?
  void SetPosition(const Position position) 
  {
    position_data_ = position;
  }
  
  void SetDilution(const Dilution dilution) 
  {
    dilution_data_ = dilution;
  }

  void SetAlmanac(const Almanac almanac) 
  {
    almanac_ = almanac;
  }

  void SetTimestamp(const Timestamp timestamp) 
  {
    timestamp_ = timestamp;
  }
  
  void SetTrackingSatellites(const int32_t tracking_satellites)
  {
    tracking_satellites_ = tracking_satellites;
  }

  void SetVisibleSatellites(const int32_t visible_satellites)
  {
    visible_satellites_ = visible_satellites;
  } 

private:

  bool haslock_{};
  bool status_{};			// Status: true=active, false=void (not locked)

  Type type_{};
  Quality quality_{};

  int32_t tracking_satellites_{};
  int32_t visible_satellites_{};

  Dilution dilution_data_{};
  Position position_data_{};
  Almanac almanac_{};
  Timestamp timestamp_{};
};

} // namespace nmea
