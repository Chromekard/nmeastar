/**
 * @file position.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

namespace nmeastar::gps
{

/**
 * Data structure containing gps position data, travel speed and direction
*/
struct Position
{
  double altitude_{};		  // meters
  double latitude_{};		  // degrees N
  double longitude_{};		// degrees E
  double speed_{};				// km/h
  double travel_angle_{}; // degrees true north (0-360)
};

}