/**
 * @file service.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 14, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <string>
#include <functional>
#include <list>
#include <mutex>
#include <shared_mutex>

#include "nmeastar/gps/fix.h"
#include "nmeastar/gps/watch/subject.h"
#include "nmeastar/nmea/reader.h"

namespace nmeastar::gps
{

/**
 * Gps service class.
 * This class contains functions for interpreting nmea sentences and
 * updating the local fix based on the functions triggered. This class
 * also has a list of observer objects that can be notified when data changes.
 * The class is multithread secure.
*/
class Service : public Subject
{
public:

  Service(nmea::Reader &reader);
  virtual ~Service() = default;

  /**
   * @todo Maybe change parameters to enum or something easier to handle?
   * Public callback function to get error information
  */
  std::function<void(std::string)> OnError;

  /**
   * Attaches the default nmea sentence functions to the reader
   * @param reader to attach to
  */
  void AttachToReader(nmea::Reader &reader);

  /**
   * Force notify all attached observers by sending an lock state message
  */
  void NotifyLock() override;

  /**
   * Force notify all attached observers by sending an update message
  */
  void NotifyFix() override;

  /**
   * @note do not call from observer callback as it will cause dead lock
   * Get a const copy of the fix for reading values
   * @return const copy of GPS fix
   * MT Safe
  */
  const Fix GetFix()
  {
    std::shared_lock lock(mtx_);
    return fix_;
  }

  /**
   * Check the internal gps has locked on to a satelite fix
   * @return true if locked on, false if not 
  */
  bool Locked()
  {
    std::shared_lock lock(mtx_);
    return fix_.Locked();
  }

  /**
   * Get the position data of the internal fix
   * @return position
   * @note Use to prevent returning a full fix copy when
   * we only need position data
   * @see fix.h for more information
   * MT Safe
  */
  const Position GetPosition()
  {
    std::shared_lock<std::shared_mutex> lock(mtx_);
    return fix_.GetPosition();
  }

  /**
   * Get the dilution data of the internal fix
   * @return dilution
   * @note Use to prevent returning a full fix copy when
   * we only need position data
   * @see fix.h for more information
   * MT Safe
  */
  const Dilution GetDilution()
  {
    std::shared_lock<std::shared_mutex> lock(mtx_);
    return fix_.GetDilution();
  }

  /**
   * Get the timestamp data of the internal fix
   * @return timestamp
   * @note Use to prevent returning a full fix copy when
   * we only need position data
   * @see fix.h for more information
   * MT Safe
  */
  const Timestamp GetTimestamp()
  {
    std::shared_lock<std::shared_mutex> lock(mtx_);
    return fix_.GetTimestamp();
  }

  ///TODO: Consider if certain data can be grouped?
  ///Like position and dilution for accuracy checking
  ///


protected:
  Fix fix_{};

  //Mutex for locking data in a multi threaded environment
  mutable std::shared_mutex mtx_{};

private:

  void Read_PSRF150(const nmea::Sentence &nmea);
  void Read_GPGGA(const nmea::Sentence &nmea);
  void Read_GPGSA(const nmea::Sentence &nmea);
  void Read_GPGSV(const nmea::Sentence &nmea);
  void Read_GPRMC(const nmea::Sentence &nmea);
  void Read_GPVTG(const nmea::Sentence &nmea);
};

} // namespace nmea
