/**
 * @file command.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Sep 8, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#pragma once

#include <string>

namespace nmeastar::nmea::command
{

/**
 * Base NMEA command class
 * Used for 
*/
class Command
{
public:
  Command(){};
  Command(std::string name, std::string message) : name_(name), message_(message){};

  virtual ~Command() = default;

  /**
   * 
  */
  virtual std::string ToString();

  /**
   * 
  */
  std::string AddChecksum(const std::string &s);

protected:
  std::string name_{};
  std::string message_{};
  char checksum_{};
};

} // namespace nmea
