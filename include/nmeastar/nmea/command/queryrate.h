/**
 * @file queryrate.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Sep 8, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#pragma once

#include "nmeastar/nmea/command/command.h"

namespace nmeastar::nmea::command
{

// data fields that will be stringed.

//  $PSRF103,00,01,00,01*25
/*
  * Table 2-9 Query/Rate Control Data Format
  Name		Example		Unit Description
  Message ID	$PSRF103	PSRF103 protocol header
  Msg			00 			See Table 2-10
  Mode		01 			0=SetRate, 1=Query
  Rate		00 			sec Output—off=0, max=255
  CksumEnable 01 			0=Disable Checksum, 1=Enable Checksum
  Checksum	*25
  <CR> <LF> End of message termination
  */
class QueryRate : public Command
{
public:

  /**
  * Table 2-10 Messages
  *	Value Description
  *	0 GGA
  *	1 GLL
  *	2 GSA
  *	3 GSV
  *	4 RMC
  *	5 VTG
  *	6 MSS (If internal beacon is supported)
  *	7 Not defined
  *	8 ZDA (if 1PPS output is supported)
  *	9 Not defined
  */
  enum class MessageID
  { // These ID's are according to NMEA standard.
    Unknown = -1,
    GGA = 0,
    GLL = 1,
    GSA = 2,
    GSV = 3,
    RMC = 4,
    VTG = 5, // notice missing 6,7
    ZDA = 8
  };

  enum class QueryRateMode
  {
    SETRATE = 0,
    QUERY = 1
  };

  QueryRate() : Command(){};
  ///TODO: Make contsructor that takes arguments

  virtual std::string ToString();

private:
  MessageID message_id_{MessageID::Unknown};		//Message ID			$PSRF103	PSRF103 protocol header
  QueryRateMode mode_{QueryRateMode::SETRATE};	//Mode						01 				0=SetRate, 1=Query
  uint8_t rate_{};															//Rate						00 				sec Output—off=0, max=255
  bool checksum_enable_{1};											//ChechsumEnable 	01 				0=Disable Checksum, 1=Enable Checksum

};

}