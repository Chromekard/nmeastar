/**
 * @file serialconfiguration.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Sep 8, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#pragma once

#include "nmeastar/nmea/command/command.h"

namespace nmeastar::nmea::command
{

class SerialConfiguration : public Command
{
public:

  SerialConfiguration() : Command("PSRF100", {}){};

  SerialConfiguration(int32_t baud, int32_t data_bits, int32_t stop_bits, int32_t parity) : 
    Command("PSRF100", {}), baud_(baud), data_bits_(data_bits),
    stop_bits_(stop_bits), parity_(parity){};

  virtual std::string ToString();

private:
  int32_t baud_{4800};	 //4800, 9600, 19200, 38400
  int32_t data_bits_{8}; //7, 8 Databits
  int32_t stop_bits_{1}; //0, 1 Stopbits
  int32_t parity_{};		 //0=none, 1=odd, 2=even Parity
};

}