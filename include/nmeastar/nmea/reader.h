/**
 * @file reader.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 12, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#pragma once

#include <string>
#include <functional>
#include <unordered_map>
#include <cstdint>

#include "sentence.h"

//read class definition for info
#define NMEA_PARSER_MAX_BUFFER_SIZE 2000

namespace nmeastar::nmea
{
  
/**
 * Class for reading raw data into raw NMEA strings with an internal event map
 * for handling created NMEA Sentences
*/
class Reader
{
public:
  Reader(){};
  virtual ~Reader() = default;

  //Public function that is called every time reader receives any NMEA sentence
  std::function<void(const Sentence &)> OnSentence;

  /**
   * Adds a handler function to the internal event table. 
   * The function will be called any time a sentence with the same name as the key is found.
   * @param key used to store and call the handler function. Needs to match a NMEASentence name
   * @param handler function to add to the internal event table.
  */
  void SetSentenceHandler(std::string key, std::function<void(const Sentence &)> handler);

  /**
   * Show a list of NMEA sentence names that currently have handlers.
   * @return comma seperated list of registered nmea sentence names.
  */
  std::string GetRegisteredSentenceHandlersCSV();

  // ########## Byte streaming functions ########## //

  /**
   * Read a single byte of data
   * @param byte to read
  */
  void ReadByte(uint8_t byte);

  /**
   * Read a buffer of data
   * @param bytes to read
   * @param size of buffer
  */
  void ReadBuffer(uint8_t *bytes, uint32_t size);

  /**
   * Read a line of string data
   * @param line of data
  */
  void ReadLine(std::string line);

  /**
   * Reads an a complete NMEA sentence
   * Called when parser receives a sentence from the byte stream. Can also be called by user to inject sentences.
   * If not an empty sentence this will trigger OnSentence function and if found a function in the event table
   * @param raw_sentence to convert to nmea sentence
   * @return NMEA sentence created from raw sentence
  */
  Sentence ReadSentence(std::string raw_sentence);

private:
  std::unordered_map<std::string, std::function<void(Sentence)>> event_table_{};
  std::string buffer_{};
  bool filling_buffer_{};
  uint32_t max_buffer_size_{NMEA_PARSER_MAX_BUFFER_SIZE}; //limit the max size if no newline ever comes... Prevents huge buffer string internally
};

} // namespace nmea
