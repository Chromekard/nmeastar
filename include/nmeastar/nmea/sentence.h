/**
 * @file sentence.h
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#pragma once

#include <string>
#include <vector>
#include <tuple>

namespace nmeastar::nmea
{

/**
 * Class that represents a nmea sentence.
 * Used to parse nmea sentence into its components.
*/
class Sentence
{
public:

  /**
   * Enum class represeting different errors that occure during parsing
   * Also includes a none case if no errors happened.
  */
  enum class ParseError
  {
    None,
    EmptyText,
    MissingDollar,
    InvalidName,
    MissingName,
    MissingInformation,
    MissingChecksum,
    InvalidParameter,
    MissingChecksumData
  };

  /**
   * Calculate checksum from string
   * @param str to calculate
   * @return checksum
  */
  static uint8_t CalculateChecksum(std::string str); // returns checksum of string -- XOR

  /**
   * Get the parse error as text
   * @param error
   * @return error description
  */
  static std::string GetErrorText(ParseError error);

  /**
   * Construct a NMEASentence from string
   * @param txt to parse to NMEASentence
  */
  Sentence(std::string txt);
  virtual ~Sentence() = default;


  /**
   * Check that the sentence checksum exists and that the parsed
   * checksum matches the calculated checksum
   * @return false if checksum does not exist or if parsed and calculated checksums do not match, true otherwise
  */
  bool ChecksumOK() const {return (has_checksum_) && (parsed_checksum_ == calculated_checksum_);}

  /**
   * Check if the sentence is valid
   * @return true if no errors occured during parsing, false otherwise
  */
  bool Valid() const {return parse_error_ == ParseError::None;}

  /**
   * Check if sentence has checksum
   * @return true if checksum exists, false if not
  */
  bool HasCheckSum() const {return has_checksum_;}

  /**
   * Get parse error
   * @return ParseError enum
  */
  ParseError GetParseError() const {return parse_error_;}

  /**
   * Get the raw sentence that was parsed
   * @return raw sentence
  */
  std::string GetRawSentence() const { return raw_sentence_; }

  /**
   * Get the sentence command name
   * @return command name
  */
  std::string GetCommandName() const { return command_name_;	}

  /**
   * Get the sentence parameters
   * @return parameters
  */
  std::vector<std::string> GetParameters() const { return parameters_; }

private:

  std::string raw_sentence_{};
  std::string command_name_{};
  std::vector<std::string> parameters_{};

  bool has_checksum_{};

  uint8_t parsed_checksum_{};
  uint8_t calculated_checksum_{};
  
  ParseError parse_error_{ParseError::EmptyText};
};

} // namespace nmea