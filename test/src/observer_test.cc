 
#include <iostream>
#include <fstream>

#include <gtest/gtest.h>

#include "nmeastar/nmeastar.h"

TEST(Observer_Test, Get_updates)
{
  nmeastar::nmea::Reader reader{};
	nmeastar::gps::Service gps(reader);
  bool lock{};
  nmeastar::gps::Position pos_data{};

  nmeastar::gps::Observer observer{ gps,
    [&lock](bool state)
    {
      lock = state;
    }, 
    [&pos_data](const nmeastar::gps::Fix& fix)
    {
      pos_data = fix.GetPosition();
    }};

  std::string line;
	std::ifstream file("../../nmea_log.txt");
	while (std::getline(file, line)){
		reader.ReadLine(line);
	}

  ASSERT_TRUE(pos_data.speed_ != 0.0);
}