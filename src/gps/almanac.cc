/**
 * @file almanac.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Jul 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/gps/almanac.h"

#include "nmeastar/gps/satellite.h"
 
double nmeastar::gps::Almanac::PercentComplete() const
{
  if (total_pages_ == 0)
  {
    return 0.0;
  }

  return (static_cast<double>(processed_pages_)) / (static_cast<double>(total_pages_)) * 100.0;
}

double nmeastar::gps::Almanac::AverageSNR() const
{
  double avg{0};
  double relevant{0};
  for (const auto &satellite : satellites_)
  {
    if (satellite.GetSNR() > 0)
    {
      relevant += 1.0;
    }
  }

  for (const auto &satellite : satellites_)
  {
    if (satellite.GetSNR() > 0)
    {
      avg += satellite.GetSNR() / relevant;
    }
  }

  return avg;
}

double nmeastar::gps::Almanac::MinSNR() const
{
  double min{9999999};
  if (satellites_.empty())
  {
    return 0;
  }
  int32_t num_over_zero{0};
  for (const auto &satellite : satellites_)
  {
    if (satellite.GetSNR() > 0)
    {
      num_over_zero++;
      if (satellite.GetSNR() < min)
      {
        min = satellite.GetSNR();
      }
    }
  }
  if (num_over_zero == 0)
  {
    return 0;
  }
  return min;
}

double nmeastar::gps::Almanac::MaxSNR() const
{
  double max{};
  for (const auto &satellite : satellites_)
  {
    if (satellite.GetSNR() > 0)
    {
      if (satellite.GetSNR() > max)
      {
        max = satellite.GetSNR();
      }
    }
  }
  return max;
}

void nmeastar::gps::Almanac::Clear()
{
  last_page_ = 0;
  total_pages_ = 0;
  processed_pages_ = 0;
  visible_size_ = 0;
  satellites_.clear();
}

void nmeastar::gps::Almanac::UpdateSatellite(gps::Satellite sat)
{
  if (satellites_.size() > visible_size_)
  { //we missed the new almanac start page, start over.
    Clear();
  }

  satellites_.push_back(sat);
}