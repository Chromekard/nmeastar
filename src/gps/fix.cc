/**
 * @file fix.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Jul 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/gps/fix.h"

#include <array>
#include <sstream>
#include <iomanip>
#include <cmath>

#include "nmeastar/gps/almanac.h"
#include "nmeastar/gps/satellite.h"
#include "nmeastar/gps/timestamp.h"

/**
 * anonymous namespace
*/
namespace
{
	std::string FixStatusToString(char status)
	{
		switch (status)
		{
		case true:
			return "Active";
		case false:
			return "Void";
		default:
			return "Unknown";
		}
	}

	std::string FixTypeToString(nmeastar::gps::Fix::Type type)
	{
		switch (type)
		{
		case nmeastar::gps::Fix::Type::none:
			return "None";
		case nmeastar::gps::Fix::Type::_2d:
			return "2D";
		case nmeastar::gps::Fix::Type::_3d:
			return "3D";
		default:
			return "Unknown";
		}
	}

	std::string FixQualityToString(nmeastar::gps::Fix::Quality quality)
	{
		switch (quality)
		{
		case nmeastar::gps::Fix::Quality::invalid:
			return "Invalid";
		case nmeastar::gps::Fix::Quality::gps_fix:
			return "Standard";
		case nmeastar::gps::Fix::Quality::dgps_fix:
			return "DGPS";
		case nmeastar::gps::Fix::Quality::pps_fix:
			return "PPS fix";
		case nmeastar::gps::Fix::Quality::rtk:
			return "Real Time Kinetic";
		case nmeastar::gps::Fix::Quality::float_rtk:
			return "Real Time Kinetic (float)";
		case nmeastar::gps::Fix::Quality::estimated:
			return "Estimate";
		default:
			return "Unknown";
		}
	}
} // namespace

// Takes a degree travel heading (0-360') and returns the name
std::string nmeastar::gps::Fix::TravelAngleToCompassDirection(double deg, bool abbrev)
{
	//normalize, just in case
	int32_t c = static_cast<int32_t>(std::round(deg / 360.0 * 8.0));
	int32_t r = c % 8;
	if (r < 0)
	{
		r = 8 + r;
	}

	if (abbrev)
	{
		std::array<std::string, 9> dirs = {
				"N",
				"NE",
				"E",
				"SE",
				"S",
				"SW",
				"W",
				"NW",
				"N"};
		return dirs[r];
	}
	else
	{
		std::array<std::string, 9> dirs = {
				"North",
				"North East",
				"East",
				"South East",
				"South",
				"South West",
				"West",
				"North West",
				"North"};
		return dirs[r];
	}
}

nmeastar::gps::Fix::Fix() : almanac_({}), timestamp_({})
{
}

// Returns the duration since the Host has received information
std::chrono::seconds nmeastar::gps::Fix::TimeSinceLastUpdate() const
{
	time_t now = time(nullptr);
	uint64_t secs = static_cast<uint64_t>(difftime(now, timestamp_.GetTime()));
	return std::chrono::seconds(static_cast<uint64_t>(secs));
}

std::string nmeastar::gps::Fix::ToString(const Precision& chip_precision) const
{
	std::stringstream ss{};
	std::ios_base::fmtflags oldflags = ss.flags();

	ss << "========================== GPS FIX ================================" << std::endl
		 << " Status: \t\t" << ((haslock_) ? "LOCK!" : "SEARCHING...") << std::endl
		 << " Satellites: \t\t" << tracking_satellites_ << " (tracking) of " << visible_satellites_ << " (visible)" << std::endl
		 << " < Fix Details >" << std::endl
		 << "   Age:                " << TimeSinceLastUpdate().count() << " s" << std::endl
		 << "   timestamp_:          " << timestamp_.ToString() << "   UTC   \n\t\t\t(raw: " << timestamp_.GetRawTime() << " time, " << timestamp_.GetRawDate() << " date)" << std::endl
		 << "   Raw Status:         " << status_ << "  (" << ::FixStatusToString(status_) << ")" << std::endl
		 << "   Type:               " << static_cast<int>(type_) << "  (" << ::FixTypeToString(type_) << ")" << std::endl
		 << "   Quality:            " << static_cast<int>(quality_) << "  (" << ::FixQualityToString(quality_) << ")" << std::endl
		 << "   Lat/Lon (N,E):      " << std::setprecision(6) << std::fixed << position_data_.latitude_ << "' N, " << position_data_.longitude_ << "' E" << std::endl;

	ss.flags(oldflags); //reset

	ss << "   DOP (P,H,V):        " << dilution_data_.position_ << ",   " << dilution_data_.horizontal_ << ",   " << dilution_data_.vertical_ << std::endl
		 << "   Accuracy(H,V):      " << HorizontalAccuracy(chip_precision) << " m,   " << VerticalAccuracy(chip_precision) << " m" << std::endl;

	ss << "   Altitude:           " << position_data_.altitude_ << " m" << std::endl
		 << "   Speed:              " << position_data_.speed_ << " km/h" << std::endl
		 << "   Travel Dir:         " << position_data_.travel_angle_ << " deg  [" << TravelAngleToCompassDirection(position_data_.travel_angle_) << "]" << std::endl
		 << "   SNR:                avg: " << almanac_.AverageSNR() << " dB   [min: " << almanac_.MinSNR() << " dB,  max:" << almanac_.MaxSNR() << " dB]" << std::endl;

	ss << " < almanac_ (" << almanac_.PercentComplete() << "%) >" << std::endl;
	if (almanac_.satellites_.empty())
	{
		ss << " > No satellite info in almanac_." << std::endl;
	}
	for (size_t i = 0; i < almanac_.satellites_.size(); i++)
	{
		ss << "   [" << std::setw(2) << std::setfill(' ') << (i + 1) << "]   " << almanac_.satellites_[i].ToString() << std::endl;
	}

	return ss.str();
}

/*
nmeastar::gps::Fix::operator std::string()
{
	return ToString();
}
*/

bool nmeastar::gps::Fix::SetLock(bool locked)
{
	if (haslock_ != locked)
	{
		haslock_ = locked;
		return true;
	}
	return false;
}