/**
 * @file satellite.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/gps/satellite.h"

#include <sstream>
#include <iomanip>
#include <algorithm>

///TODO: Do a value check on the 

nmeastar::gps::Satellite::Satellite(uint32_t prn, double snr, double elevation, double azimuth) :
	prn_(std::clamp(prn, {0}, {32})), 
	snr_(std::clamp(snr, 0.0, 99.0)),
	elevation_((std::clamp(elevation, 0.0, 90.0))), 
	azimuth_(std::clamp(azimuth, 0.0, 359.0))
{}

std::string nmeastar::gps::Satellite::ToString() const
{
	std::stringstream ss{};
	ss << "[PRN: " << std::setw(3) << std::setfill(' ') << prn_ << " "
		 << "  SNR: " << std::setw(3) << std::setfill(' ') << snr_ << " dB  "
		 << "  Azimuth: " << std::setw(3) << std::setfill(' ') << azimuth_ << " deg "
		 << "  Elevation: " << std::setw(3) << std::setfill(' ') << elevation_ << " deg  "
		 << "]";

	return ss.str();
}

nmeastar::gps::Satellite::operator std::string()
{
	return ToString();
}

void nmeastar::gps::Satellite::SetElevation(double elevation)
{
	elevation_ = std::clamp(elevation, 0.0, 90.0);
}

void nmeastar::gps::Satellite::SetAzimuth(double azimuth)
{
	azimuth_ = std::clamp(azimuth, 0.0, 359.0);
}