/**
 * @file timestamp.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Feb 11, 2021
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/gps/timestamp.h"

#include <sstream>
#include <array>
#include <cmath>

// indexed from 1!
std::string nmeastar::gps::Timestamp::MonthName(uint32_t index) const
{
	if (index < 1 || index > 12)
	{
		std::stringstream ss{};
		ss << "[month:" << index << "]";
		return ss.str();
	}

	std::array<std::string, 12> names = {
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"};
	return names[index - 1];
}

// Returns seconds since Jan 1, 1970. Classic Epoch time.
time_t nmeastar::gps::Timestamp::GetTime() const
{
	struct tm t{};
	t.tm_year = year_ - 1900; // This is year-1900, so 112 = 2012
	t.tm_mon = month_ - 1;		 // month from 0:Jan
	t.tm_mday = day_;
	t.tm_hour = hour_;
	t.tm_min = min_;
	t.tm_sec = static_cast<int>(sec_);
	return mktime(&t);
}

void nmeastar::gps::Timestamp::SetTime(double raw_ts)
{
	raw_time_ = raw_ts;

	hour_ = static_cast<int32_t>(trunc(raw_ts / 10000.0));
	min_ = static_cast<int32_t>(trunc((raw_ts - hour_ * 10000) / 100.0));
	sec_ = raw_ts - min_ * 100 - hour_ * 10000;
}

//ddmmyy
void nmeastar::gps::Timestamp::SetDate(int32_t raw_date)
{
	raw_date_ = raw_date;
	// If uninitialized, use posix time.
	if (raw_date_ == 0)
	{
		month_ = 1;
		day_ = 1;
		year_ = 1970;
	}
	else
	{
		day_ = static_cast<int32_t>(trunc(raw_date / 10000.0));
		month_ = static_cast<int32_t>(trunc((raw_date - 10000 * day_) / 100.0));
		year_ = raw_date - 10000 * day_ - 100 * month_ + 2000;
	}
}

std::string nmeastar::gps::Timestamp::ToString() const
{
	std::stringstream ss{};
	ss << hour_ << "h " << min_ << "m " << sec_ << "s"
		 << "  " << MonthName(month_) << " " << day_ << " " << year_;
	return ss.str();
}