/**
 * @file service.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 14, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/gps/service.h"

#include <iostream>
#include <sstream>
#include <cmath>
#include <chrono>

#include "nmeastar/gps/watch/observer.h"
#include "nmeastar/utility.h"

namespace
{

	// Takes the NMEA lat/long format (dddmm.mmmm, [N/S,E/W]) and converts to degrees N,E only
	double ConvertLatLongToDeg(const std::string &llstr, const std::string &dir)
	{

		double pd = nmeastar::utility::ParseDouble(llstr);
		double deg = trunc(pd / 100); //get ddd from dddmm.mmmm
		double mins = pd - deg * 100;

		deg = deg + mins / 60.0;

		char hdg = 'x';
		if (!dir.empty())
		{
			hdg = dir[0];
		}

		//everything should be N/E, so flip S,W
		if (hdg == 'S' || hdg == 'W')
		{
			deg *= -1.0;
		}

		return deg;
	}

	double convertKnotsToKilometersPerHour(double knots)
	{
		return knots * 1.852;
	}

} // namespace

// ------------- GPSSERVICE CLASS -------------

nmeastar::gps::Service::Service(nmea::Reader &reader) : Subject()
{
	AttachToReader(reader); // attach to parser in the GPS object
}

// http://www.gpsinformation.org/dale/nmea.htm
/* used sentences...
$GPGGA		- time,position,fix_ data
$GPGSA		- gps receiver operating mode, satellites used in position and DOP values
$GPGSV		- number of gps satellites in view, satellite ID, elevation,azimuth, and SNR
$GPRMC		- time,date, position,course, and speed data
$GPVTG		- course and speed information relative to the ground
$GPZDA		- 1pps timing message
$PSRF150	- gps module "ok to send"
*/
void nmeastar::gps::Service::AttachToReader(nmeastar::nmea::Reader& reader)
{
	reader.SetSentenceHandler("PSRF150", [this](const nmea::Sentence &nmea) {
		Read_PSRF150(nmea);
	});
	reader.SetSentenceHandler("GPGGA", [this](const nmea::Sentence &nmea) {
		Read_GPGGA(nmea);
	});
	// TODO: Make sure this is 100% compatible.
	reader.SetSentenceHandler("GNGGA", [this](const nmea::Sentence &nmea) {
		Read_GPGGA(nmea);
	});
	reader.SetSentenceHandler("GPGSA", [this](const nmea::Sentence &nmea) {
		Read_GPGSA(nmea);
	});
	reader.SetSentenceHandler("GPGSV", [this](const nmea::Sentence &nmea) {
		Read_GPGSV(nmea);
	});
	reader.SetSentenceHandler("GPRMC", [this](const nmea::Sentence &nmea) {
		Read_GPRMC(nmea);
	});
	reader.SetSentenceHandler("GPVTG", [this](const nmea::Sentence &nmea) {
		Read_GPVTG(nmea);
	});
}

void nmeastar::gps::Service::NotifyLock()
{
  ///NOTE: Getting a fix copy here to avoid locking issues is for some reason callback should try to access service again
  auto state = GetFix().Locked();
  std::scoped_lock<std::mutex> lock(observer_mtx_);
  for(auto item : observer_set_)
  {
    if(item->on_state_lock_changed_)
    {
      item->on_state_lock_changed_(state);
    }
  }
}

void nmeastar::gps::Service::NotifyFix()
{
  ///NOTE: Getting a fix copy here to avoid locking issues is for some reason callback should try to access service again
  auto fix_copy = GetFix();
  std::scoped_lock<std::mutex> lock(observer_mtx_);
  for(auto item : observer_set_)
  {
    if(item->on_update_)
    {
      item->on_update_(fix_copy);
    }
  }
}

void nmeastar::gps::Service::Read_PSRF150(const nmea::Sentence &nmea)
{
  ///TODO: Implement
	// nothing right now...
	// Called with checksum 3E (valid) for GPS turning ON
	// Called with checksum 3F (invalid) for GPS turning OFF
}


/* -- EXAMPLE --
$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47

$GPGGA,205630.945,3346.1070,N,08423.6687,W,0,03,,30.8,M,-30.8,M,,0000*73		// ATLANTA!!!!

Where:
GGA          Global Positioning System fix_ Data
index:
[0] 123519       fix_ taken at 12:35:19 UTC
[1-2] 4807.038,N   Latitude 48 deg 07.038' N
[3-4] 1131.000,E  Longitude 11 deg 31.000' E
[5] 1            fix_ quality: 0 = invalid
1 = GPS fix_ (SPS)
2 = DGPS fix_
3 = PPS fix_
4 = Real Time Kinematic
5 = Float RTK
6 = estimated (dead reckoning) (2.3 feature)
7 = Manual input mode
8 = Simulation mode
[6] 08           Number of satellites being tracked
[7] 0.9          Horizontal dilution of position
[8-9] 545.4,M      Altitude, Meters, above mean sea level
[10-11] 46.9,M       Height of geoid (mean sea level) above WGS84
ellipsoid
[12] (empty field) time in seconds since last DGPS update
[13] (empty field) DGPS station ID number
[13]  *47          the checksum data, always begins with *
*/
void nmeastar::gps::Service::Read_GPGGA(const nmea::Sentence &nmea)
{
	if (!nmea.ChecksumOK())
	{
		if(OnError){OnError("Checksum is invalid!");};
		return;
	}

	if (nmea.GetParameters().size() < 14)
	{
    if(OnError){OnError("GPS data is missing parameters");};
		return;
	}


  bool lockupdate = false;
  {
    std::scoped_lock<std::shared_mutex> guard(mtx_);

    // TIMESTAMP
    fix_.timestamp_.SetTime(utility::ParseDouble(nmea.GetParameters()[0]));

    std::string sll;
    std::string dir;
    // LAT
    sll = nmea.GetParameters()[1];
    dir = nmea.GetParameters()[2];
    if (!sll.empty())
    {
      fix_.position_data_.latitude_ = ConvertLatLongToDeg(sll, dir);
    }

    // LONG
    sll = nmea.GetParameters()[3];
    dir = nmea.GetParameters()[4];
    if (!sll.empty())
    {
      fix_.position_data_.longitude_ = ConvertLatLongToDeg(sll, dir);
    }

    // fix_ QUALITY
    fix_.quality_ = static_cast<Fix::Quality>(utility::ParseInt(nmea.GetParameters()[5]));
    if (fix_.quality_ == Fix::Quality::invalid)
    {
      lockupdate = fix_.SetLock(false);
    }
    else if (fix_.quality_ == Fix::Quality::gps_fix)
    {
      lockupdate = fix_.SetLock(true);
    }
    else
    {
    }

    // TRACKING SATELLITES
    fix_.tracking_satellites_ = static_cast<int32_t>(utility::ParseInt(nmea.GetParameters()[6]));
    if (fix_.visible_satellites_ < fix_.tracking_satellites_)
    {
      fix_.visible_satellites_ = fix_.tracking_satellites_; // the visible count is in another sentence.
    }

    // ALTITUDE
    if (!nmea.GetParameters()[8].empty())
    {
      fix_.position_data_.altitude_ = utility::ParseDouble(nmea.GetParameters()[8]);
    }
    else
    {
      // leave old value
    }
  }

  ///NOTE: Getting a fix copy here to avoid locking issues is for some reason callback should try to access service again
  auto fix_copy = GetFix();

	//calling observers
	if (lockupdate)
	{
    NotifyLock();
	}

  NotifyFix();

	//NMEAParseError pe("GPS Number Bad Format [$GPGGA] :: " + ex.message, nmea);
	//NMEAParseError pe("GPS Data Bad Format [$GPGGA] :: " + ex.message, nmea);
}

/*  -- EXAMPLE --
$GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39

$GPGSA,A,3,18,21,22,14,27,19,,,,,,,4.4,2.7,3.4*32

Where:
GSA      Satellite status
[0] A        Auto selection of 2D or 3D fix_ (M = manual)
[1] 3        3D fix_ - values include: 1 = no fix_
2 = 2D fix_
3 = 3D fix_
[2-13] 04,05... PRNs of satellites used for fix_ (space for 12)
[14] 2.5      PDOP (dilution of precision)
[15] 1.3      Horizontal dilution of precision (HDOP)
[16] 2.1      Vertical dilution of precision (VDOP)
[16] *39      the checksum data, always begins with *
*/
void nmeastar::gps::Service::Read_GPGSA(const nmea::Sentence &nmea)
{
	if (!nmea.ChecksumOK())
	{
		if(OnError){OnError("Checksum is invalid!");};
		return;
	}

	if (nmea.GetParameters().size() < 17)
	{
		if(OnError){OnError("GPS data is missing parameters");};
		return;
	}

	// fix_ TYPE
	bool lockupdate = false;

  {
    std::scoped_lock<std::shared_mutex> guard(mtx_);

    ///TODO: Move this to inside the fix as setting the internal state should be done in that function
    fix_.type_ = static_cast<Fix::Type>(utility::ParseInt(nmea.GetParameters()[1]));
    if (fix_.type_ == Fix::Type::none)
    {
      lockupdate = fix_.SetLock(false);
    }
    if (fix_.type_ == Fix::Type::_3d)
    {
      lockupdate = fix_.SetLock(true);
    }

    // DILUTION OF PRECISION  -- PDOP
    double dop = utility::ParseDouble(nmea.GetParameters()[14]);
    fix_.dilution_data_.position_ = dop;

    // HORIZONTAL DILUTION OF PRECISION -- HDOP
    double hdop = utility::ParseDouble(nmea.GetParameters()[15]);
    fix_.dilution_data_.horizontal_ = hdop;

    // VERTICAL DILUTION OF PRECISION -- VDOP
    double vdop = utility::ParseDouble(nmea.GetParameters()[16]);
    fix_.dilution_data_.vertical_ = vdop;
  }

  ///TODO: This function only realy changes the lock on the dilution data, maybe add a callback for dilution?

	//calling handlers
	if (lockupdate)
	{
    NotifyLock();
	}

  NotifyFix();
	//NMEAParseError pe("GPS Number Bad Format [$GPGGA] :: " + ex.message, nmea);
	//NMEAParseError pe("GPS Data Bad Format [$GPGGA] :: " + ex.message, nmea);
}

/*  -- EXAMPLE --
$GPGSV,2,1,08,01,40,083,46,02,17,308,41,12,07,344,39,14,22,228,45*75


$GPGSV,3,1,12,01,00,000,,02,00,000,,03,00,000,,04,00,000,*7C
$GPGSV,3,2,12,05,00,000,,06,00,000,,07,00,000,,08,00,000,*77
$GPGSV,3,3,12,09,00,000,,10,00,000,,11,00,000,,12,00,000,*71

Where:
GSV          Satellites in view
[0] 2            Number of sentences for full data
[1] 1            sentence 1 of 2
[2] 08           Number of satellites in view

[3] 01           Satellite PRN number
[4] 40           Elevation, degrees
[5] 083          Azimuth, degrees
[6] 46           SNR - higher is better
[...]   for up to 4 satellites per sentence
[17] *75          the checksum data, always begins with *
*/
void nmeastar::gps::Service::Read_GPGSV(const nmea::Sentence &nmea)
{
	if (!nmea.ChecksumOK())
	{
		if(OnError){OnError("Checksum is invalid!");};
		return;
	}

	// can't do this check because the length varies depending on satallites...
	//if(nmea.GetParameters().size() < 18){
	//	throw NMEAParseError("GPS data is missing parameters.");
	//}

  {
    std::scoped_lock<std::shared_mutex> guard(mtx_);

    // VISIBLE SATELLITES
    fix_.visible_satellites_ = static_cast<int32_t>(utility::ParseInt(nmea.GetParameters()[2]));
    if (fix_.tracking_satellites_ == 0)
    {
      fix_.visible_satellites_ = 0; // if no satellites are tracking, then none are visible!
    }																	 // Also NMEA defaults to 12 visible when chip powers on. Obviously not right.

    uint32_t totalPages = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[0]));
    uint32_t currentPage = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[1]));

    //if this is the first page, then reset the almanac
    if (currentPage == 1)
    {
      fix_.almanac_.Clear();
    }

    fix_.almanac_.last_page_ = currentPage;
    fix_.almanac_.total_pages_ = totalPages;
    fix_.almanac_.visible_size_ = fix_.visible_satellites_;

    int entriesInPage = (nmea.GetParameters().size() - 3) >> 2; //first 3 are not satellite info
    //- entries come in 4-ples, and truncate, so used shift
    for (int i = 0; i < entriesInPage; i++)
    {
      int prop = 3 + i * 4;

      // PRN, ELEVATION, AZIMUTH, SNR
      auto prn = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[prop]));
      auto elevation = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[prop + 1]));
      auto azimuth = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[prop + 2]));
      auto snr = static_cast<uint32_t>(utility::ParseInt(nmea.GetParameters()[prop + 3]));

      //cout << "ADDING SATELLITE ::" << sat.toString() << endl;
      fix_.almanac_.UpdateSatellite(Satellite{prn, snr, elevation, azimuth});
    }

    fix_.almanac_.processed_pages_++;

    //
    if (fix_.visible_satellites_ == 0)
    {
      fix_.almanac_.Clear();
    }
  }

	//cout << "ALMANAC FINISHED page " << fix_.almanac_.processedPages << " of " << fix_.almanac_.totalPages << endl;

  NotifyFix();
}


/*  -- EXAMPLE ---
$GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A
$GPRMC,235957.025,V,,,,,,,070810,,,N*4B
$GPRMC,061425.000,A,3346.2243,N,08423.4706,W,0.45,18.77,060914,,,A*47

Where:
RMC          Recommended Minimum sentence C
[0] 123519       fix_ taken at 12:35:19 UTC
[1] A            Status A=active or V=Void.
[2-3] 4807.038,N   Latitude 48 deg 07.038' N
[4-5] 01131.000,E  Longitude 11 deg 31.000' E
[6] 022.4        Speed over the ground in knots
[7] 084.4        Track angle in degrees True
[8] 230394       Date - 23rd of March 1994
[9-10] 003.1,W      Magnetic Variation
[10] *6A          The checksum data, always begins with *
// NMEA 2.3 includes another field after
*/
void nmeastar::gps::Service::Read_GPRMC(const nmea::Sentence &nmea)
{
	if (!nmea.ChecksumOK())
	{
		if(OnError){OnError("Checksum is invalid!");};
		return;
	}

	if (nmea.GetParameters().size() < 11)
	{
		if(OnError){OnError("GPS data is missing parameters");};
		return;
	}

  bool lockupdate = false;
  {
    std::scoped_lock<std::shared_mutex> guard(mtx_); //Lock while writing

    // TIMESTAMP
    fix_.timestamp_.SetTime(utility::ParseDouble(nmea.GetParameters()[0]));

    std::string sll;
    std::string dir;
    // LAT
    sll = nmea.GetParameters()[2];
    dir = nmea.GetParameters()[3];
    if (!sll.empty())
    {
      fix_.position_data_.latitude_ = ConvertLatLongToDeg(sll, dir);
    }

    // LONG
    sll = nmea.GetParameters()[4];
    dir = nmea.GetParameters()[5];
    if (!sll.empty())
    {
      fix_.position_data_.longitude_ = ConvertLatLongToDeg(sll, dir);
    }

    // ACTIVE
    char status = 'V';
    if (!nmea.GetParameters()[1].empty())
    {
      status = nmea.GetParameters()[1][0];
    }
    fix_.status_ = status;
    if (status == 'V')
    {
      lockupdate = fix_.SetLock(false);
    }
    else if (status == 'A')
    {
      lockupdate = fix_.SetLock(true);
    }
    else
    {
      lockupdate = fix_.SetLock(false); //not A or V, so must be wrong... no lock
    }

    fix_.position_data_.speed_ = convertKnotsToKilometersPerHour(utility::ParseDouble(nmea.GetParameters()[6])); // received as knots, convert to km/h
    fix_.position_data_.travel_angle_ = utility::ParseDouble(nmea.GetParameters()[7]);
    fix_.timestamp_.SetDate(static_cast<int32_t>(utility::ParseInt(nmea.GetParameters()[8])));
  }

  ///NOTE: Getting a fix copy here to avoid locking issues is for some reason callback should try to access service again
  auto fix_copy = GetFix();

  //calling handlers
  if (lockupdate)
  {
    NotifyLock();
  }

  NotifyFix();
}

/*
$GPVTG,054.7,T,034.4,M,005.5,N,010.2,K*48

where:
VTG          Track made good and ground speed
[0-1]	054.7,T      True track made good (degrees)
[2-3]	034.4,M      Magnetic track made good
[4-5]	005.5,N      Ground speed, knots
[6-7]	010.2,K      Ground speed, Kilometers per hour
[7]	*48          Checksum
*/
void nmeastar::gps::Service::Read_GPVTG(const nmea::Sentence &nmea)
{
	if (!nmea.ChecksumOK())
	{
		if(OnError){OnError("Checksum is invalid!");};
		return;
	}

	if (nmea.GetParameters().size() < 8)
	{
		if(OnError){OnError("GPS data is missing parameters");};
		return;
	}

  {
    std::scoped_lock<std::shared_mutex> guard(mtx_);
    // SPEED
    // if empty, is converted to 0
    fix_.position_data_.speed_ = utility::ParseDouble(nmea.GetParameters()[6]); //km/h
  }

  NotifyFix();
}
