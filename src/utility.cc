/**
 * @file utility.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 14, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

# include "nmeastar/utility.h"

#include <sstream>

// Note: both parseDouble and parseInt return 0 with "" input.

double nmeastar::utility::ParseDouble(const std::string &s)
{

  char *p;
  double d = ::strtod(s.c_str(), &p);
  if (*p != 0)
  {
    std::stringstream ss{};
    ss << "NumberConversionError: parseDouble() error in argument \"" << s << "\", '"
        << *p << "' is not a number.";
    //throw NumberConversionError(ss.str());
  }
  return d;
}

int64_t nmeastar::utility::ParseInt(const std::string &s, int radix)
{
  char *p;

  int64_t d = ::strtoll(s.c_str(), &p, radix);

  if (*p != 0)
  {
    std::stringstream ss;
    ss << "NumberConversionError: parseInt() error in argument \"" << s << "\", '"
        << *p << "' is not a number.";
    //throw NumberConversionError(ss.str());
  }
  return d;
}