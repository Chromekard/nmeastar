/**
 * @file reader.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Aug 12, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */
#include "nmeastar/nmea/reader.h"

#include <sstream>
#include <iostream>
#include <algorithm>
#include <cctype>
#include <vector>

#include "nmeastar/nmea/sentence.h"

namespace
{
	// remove all whitespace
	void squish(std::string &str)
	{

		char chars[] = {'\t', ' '};
		for (const char i : chars)
		{
			// needs include <algorithm>
			str.erase(std::remove(str.begin(), str.end(), i), str.end());
		}
	}

	// remove side whitespace
  /*
	void trim(std::string &str)
	{
		std::stringstream trimmer;
		trimmer << str;
		str.clear();
		trimmer >> str;
	}
  */
} // namespace

void nmeastar::nmea::Reader::SetSentenceHandler(std::string cmdKey, std::function<void(const Sentence &)> handler)
{
	event_table_.erase(cmdKey);
	event_table_.insert({cmdKey, handler});
}

std::string nmeastar::nmea::Reader::GetRegisteredSentenceHandlersCSV()
{
	if (event_table_.empty())
	{
		return "";
	}

	std::stringstream ss{};
	for (const auto &table : event_table_)
	{
		ss << table.first;
		if (!table.second)
		{
			ss << "(not callable)";
		}
		ss << ",";
	}

	std::string s = ss.str();
	if (!s.empty())
	{
		s.resize(s.size() - 1); // chop off comma
	}
	return s;
}

void nmeastar::nmea::Reader::ReadByte(uint8_t b)
{
	uint8_t startbyte = '$';
	if (filling_buffer_)
	{
		//End of sentence
		if (b == '\n')
		{
			buffer_.push_back(b);
			ReadSentence(buffer_);
			buffer_.clear();
			filling_buffer_ = false;
		}
		else
		{
			if (buffer_.size() < max_buffer_size_)
			{
				buffer_.push_back(b);
			}
			else
			{
				buffer_.clear(); //clear the host buffer so it won't overflow.
				filling_buffer_ = false;
			}
		}
	}
	else
	{
		if (b == startbyte)
		{ // only start filling when we see the start byte.
			filling_buffer_ = true;
			buffer_.push_back(b);
		}
	}
}

void nmeastar::nmea::Reader::ReadBuffer(uint8_t *b, uint32_t size)
{
	for (uint32_t i = 0; i < size; ++i)
	{
		ReadByte(b[i]);
	}
}

void nmeastar::nmea::Reader::ReadLine(std::string cmd)
{
	cmd += "\r\n";
	for (const char i : cmd)
	{
		ReadByte(i);
	}
}

// takes a complete NMEA std::string and gets the data bits from it,
// calls the corresponding handler in eventTable, based on the 5 letter sentence code
nmeastar::nmea::Sentence nmeastar::nmea::Reader::ReadSentence(std::string raw_sentence)
{
	if (raw_sentence.empty())
	{
		return Sentence{raw_sentence};
	}

	// If there is a newline at the end (we are coming from the byte reader)
	if (*(raw_sentence.end() - 1) == '\n')
	{
		if (*(raw_sentence.end() - 2) == '\r')
		{ // if there is a \r before the newline, remove it.
			raw_sentence = raw_sentence.substr(0, raw_sentence.size() - 2);
		}
		else
		{
			//Malformed newline, missing carriage return (\\r)
			raw_sentence = raw_sentence.substr(0, raw_sentence.size() - 1);
		}
	}

	// Remove all whitespace characters.
	squish(raw_sentence);

	// Seperates the data now that everything is formatted
	Sentence sentence{raw_sentence};

	// Call the "any sentence" event handler, even if invalid checksum, for possible logging elsewhere.
  if(OnSentence)
  {
    OnSentence(sentence);
  }

	// Call event handlers based on map entries
	std::function<void(const Sentence &)> handler = event_table_[sentence.GetCommandName()];
	if (handler)
	{
		handler(sentence);
	}

	return sentence;
}
