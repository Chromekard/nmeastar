/**
 * @file sentence.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Jul 23, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/nmea/sentence.h"

#include <sstream>

#include "nmeastar/utility.h"

namespace
{
	// true if the text contains a non-alpha numeric value
	bool HasNonAlphaNum(std::string txt)
	{
		for (const char i : txt)
		{
			if (!isalnum(i))
			{
				return true;
			}
		}
		return false;
	}

	// true if alphanumeric or '-'
	bool ValidParamChars(std::string txt)
	{
		for (const char i : txt)
		{
			if (!isalnum(i))
			{
				if (i != '-' && i != '.')
				{
					return false;
				}
			}
		}
		return true;
	}

} // namespace


// takes the std::string *between* the '$' and '*' in nmea sentence,
// then calculates a rolling XOR on the bytes
uint8_t nmeastar::nmea::Sentence::CalculateChecksum(std::string s)
{
	uint8_t checksum = 0;
	for (const char i : s)
	{
		checksum ^= i;
	}
	return checksum;
}


std::string nmeastar::nmea::Sentence::GetErrorText(ParseError error)
{
	switch (error)
	{
	case ParseError::None:
		return "";
	case ParseError::EmptyText:
		return "Raw text was empty";
	case ParseError::MissingDollar:
		return "No dollar sign in sentance";
	case ParseError::InvalidName:
		return "The cammand name in not valid";
	case ParseError::MissingName:
		return "The command name is missing";
	case ParseError::MissingInformation:
		return "The sentence contains no data";
	case ParseError::MissingChecksum:
		return "Checksum could not be found";
	case ParseError::InvalidParameter:
		return "One of the paramters were invalid";
	case ParseError::MissingChecksumData:
		return "The checksum data is missing";
	default:
		return "";
	}
}

nmeastar::nmea::Sentence::Sentence(std::string txt)
{
	// Return default
	if (txt.empty())
	{ 
		return; 
	}

	parse_error_ = ParseError::None;
	raw_sentence_ = txt;	// save the received text of the sentence

	// Looking for index of last '$'
	size_t start_byte = txt.find_last_of('$');
	if (start_byte == std::string::npos)
	{
		parse_error_ = ParseError::MissingDollar;
		return;
	}

	// Remove data up to last'$'
	txt = txt.substr(start_byte + 1);

	// Look for checksum
	size_t checksum_str_index = txt.find_last_of('*');
	has_checksum_ = checksum_str_index != std::string::npos;
	if (has_checksum_)
	{
		//A checksum was passed in the message, so calculate what we expect to see
		calculated_checksum_ = CalculateChecksum(txt.substr(0, checksum_str_index));

		//Check that there are at least 2 characters after *
		if(checksum_str_index + 2 > txt.size() - 1)
		{
			//Checksum '*' character at end, but no data
			parse_error_ = ParseError::MissingChecksumData;
			return;
		}

		//Extract checksum from text
		std::string checksum = txt.substr(checksum_str_index + 1, 2); 	// Extract *hh without *
		parsed_checksum_ = static_cast<uint8_t>(utility::ParseInt(checksum, 16));// convert hex string to int

		//Remove data after and including * from string
		txt = txt.substr(0, checksum_str_index);
	}
	
	//No checksum is ok as some devices allow sending data with no checksum.
	//If no checksum is disabled an extra comma at end of sentence is actually standard.
	///TODO: Maybe add a warning regarding missing checksum

	// Handle comma edge cases
	size_t comma = txt.find(',');
	if (comma == std::string::npos)
	{ 
		//No comma, but check if there is a name, "$GGGG"
		if (txt.empty())
		{
			parse_error_ = ParseError::MissingInformation;
			return;
		}

		//Extract command name
		command_name_ = txt;
		if (HasNonAlphaNum(command_name_))
		{
			parse_error_ = ParseError::InvalidName;
			return;
		}

		//Return sentence containing only name
		return;
	}

	//"$," case - no name
	if (comma == 0)
	{
		parse_error_ = ParseError::MissingName;
		return;
	}

	//name should not include first comma
	command_name_ = txt.substr(0, comma);
	if (HasNonAlphaNum(command_name_))
	{
		parse_error_ = ParseError::InvalidName;
		return;
	}

	//comma is the last character/only comma, "$GGGG,"
	if (comma == txt.size() - 1)
	{
		///TODO: What function does this serve?
		parameters_.push_back("");
		return;
	}

	//move to data after first comma
	txt = txt.substr(comma + 1, txt.size() - (comma + 1));

	//parse parameters according to csv
	std::string s{};
	std::stringstream f(txt);
	while (getline(f, s, ','))
	{
		parameters_.push_back(s);
	}

	//above line parsing does not add a blank parameter if there is a comma at the end...
	// so do it here.
	if (txt[txt.size() -1] == ',')
	{
		parameters_.emplace_back("");
	}

	for (size_t i = 0; i < parameters_.size(); i++)
	{
		if (!ValidParamChars(parameters_[i]))
		{
			parse_error_ = ParseError::InvalidParameter;
			return;
		}
	}
}