/**
 * @file command.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Sep 8, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/nmea/command/command.h"

#include <sstream>
#include <iomanip>

#include "nmeastar/nmea/sentence.h"

std::string nmeastar::nmea::command::Command::ToString()
{
	return AddChecksum(message_);
}

std::string nmeastar::nmea::command::Command::AddChecksum(const std::string &s)
{
	std::stringstream zz{};
	zz << name_ << "," << s;
	checksum_ = Sentence::CalculateChecksum(zz.str());

	std::stringstream ss{};
	std::ios_base::fmtflags oldflags = ss.flags();
	ss << "$" << zz.str() << "*" << std::hex << std::uppercase << std::internal 
		<< std::setfill('0') << std::setw(2) << static_cast<int>(checksum_) << "\r\n";
	ss.flags(oldflags); //reset

	return ss.str();
}