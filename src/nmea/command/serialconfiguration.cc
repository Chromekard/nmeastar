/**
 * @file serialconfiguration.cc
 * @author Cameron Karlsson
 * @author Chuvi-w
 * @author Bjørn Fuglestad
 * @date Sep 8, 2014
 * 
 * @section LICENCE
 * Zlib license 
 * See the license file included with this source for full text
 */

#include "nmeastar/nmea/command/serialconfiguration.h"

#include <sstream>

/*
  // $PSRF100,0,9600,8,1,0*0C

  Table 2-4 Set Serial Port Data Format
  Name		Example		Unit Description
  Message ID	$PSRF100	PSRF100 protocol header
  Protocol	0 			0=SiRF binary, 1=NMEA
  Baud 		9600 		1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  DataBits	8 			8,71

  StopBits	1 			0,1		1. SiRF protocol is only valid for 8 data bits, 1stop bit, and no parity.
  Parity		0 			0=None, 1=Odd, 2=Even
  Checksum	*0C
  <CR> <LF> End of message termination
  */

/*
  // $PSRF100,0,9600,8,1,0*0C

  Table 2-4 Set Serial Port Data Format
    Name		Example		Unit Description
    M
    Baud 		9600 		essage ID	$PSRF100	PSRF100 protocol header
    Protocol	0 			0=SiRF binary, 1=NMEA1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
    DataBits	8 			8,71
  
    StopBits	1 			0,1		1. SiRF protocol is only valid for 8 data bits, 1stop bit, and no parity.
    Parity		0 			0=None, 1=Odd, 2=Even
    Checksum	*0C
    <CR> <LF> End of message termination
  */
std::string nmeastar::nmea::command::SerialConfiguration::ToString()
{
	std::stringstream ss{};
	ss << "1," << baud_ << "," << data_bits_ << "," << stop_bits_ << "," << parity_;
	message_ = ss.str();
	return Command::AddChecksum(message_);
}